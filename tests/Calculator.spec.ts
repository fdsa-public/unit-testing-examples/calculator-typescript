import { Calculator } from "../src/Calculator";
import { expect } from "chai";

describe("Calculator tests", () => {
    it("should calculate the sum of the two parameters when Add", () => {
        const calculator = new Calculator();
        
        const result = calculator.add(1, 2);

        expect(result).to.be.equal(3);
    });

    it("should calculate the difference between the two parameters when Substract", () => {
        const calculator = new Calculator();
        
        const result = calculator.substract(1, 2);

        expect(result).to.be.equal(-1);
    });
})